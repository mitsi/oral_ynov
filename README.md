# Quick Start for Ynov Presentation
This project is an explanation of dissertation available in French here : https://www.mitsi.ovh/final_exam
This project is not intended to work in production. It works in full local, runners are not intended to be used like that and the sonar is not linked to a database (easily done if you want to). Futhermore minikube is only for tests and training.

## Installation
Docker community on Ubuntu 18.04 :
>https://docs.docker.com/install/linux/docker-ce/ubuntu/

Allow non root user to use docker
> https://docs.docker.com/install/linux/linux-postinstall/

Enable docker on start up
> sudo systemctl enable docker

Docker Gitlab CE installation & Gitlab Runner
> https://www.sheevaboite.fr/articles/installer-gitlab-5-minutes-docker/ 
> https://www.sheevaboite.fr/articles/installer-gitlab-ci-moins-5-minutes-docker/

--> I had a lots problems with the runner, I finally installed it on a virtual box ubuntu 18.04 server

- $ mkdir -p /data/gitlab/{config,data,logs}
- $ sudo mkdir -p /data/gitlab/runner
- $ sudo mkdir -p /data/registry
- $ sudo mkdir -p /data/maria
- docker-compose up
- Connect to localhost:8080, change the root password
- Add your SSH key to your account

The docker instance is configured to use SSH via 2222 port, so configure the ~/.ssh/config file like this :
<pre>
Host localhost_domain.fr
   Port 2222
   Identityfile ~/ssh/id_rsa
</pre>
 
 Optional :Configure /etc/hosts and add a reverse proxy :
/etc/hosts
<pre>
127.0.0.1       localhost_domain.fr
127.0.0.1       gitlab.localhost_domain.fr
127.0.0.1       sonar.localhost_domain.fr
</pre>

install nginx :
> sudo apt install nginx

create the file /etc/nginx/sites-available/proxy_gitlab_local
>  sudo nano /etc/nginx/sites-available/proxy_gitlab_local
<pre>
server {
  listen 80;
  server_name gitlab.localhost_domain.fr;

  location / {
    proxy_pass http://0.0.0.0:8081;
    proxy_set_header        X-Real-IP  $remote_addr;
  }
}

server {                                                                                                                                                                                     
  listen 80;                                                                                                                                                                                 
  server_name sonar.localhost_domain.fr;                                                                                                                                                     

  location / {
    proxy_pass http://0.0.0.0:9000;
    proxy_set_header        X-Real-IP  $remote_addr;
  }
}
</pre>
create a symlink in the sites-enabled forlder
> sudo ln -s /etc/nginx/sites-available/proxy_gitlab_local /etc/nginx/sites-enabled/

restart the nginx service
> sudo systemctl restart nginx

Create a project on Gitlab (here oral) then clone it.
>    git clone git@localhost_domain.fr:root/oral.git

In order to use a runner (the runner cannot clone the repo due to DNS issue) in docker-compose environement, we need to add the custom network. Addt this line in /etc/gitlab-runner/config.toml 
<pre>
  ...
  [runners.docker]
    tls_verify = false
    image = "alpine:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
    network_mode = "oral_gitlab"
    ...
</pre>

SonarQube
- Default password : admin:admin
- Go to administration, market place and install the Gitlab plugin
	- Once done, restart SonarQube
- Then go to gitlab, create an access token
- Configure in SonarQube, Gitlab plugin, fill the URI and token

## Kub
> https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/

## Python
Active venv with the script: start_venv.sh
> ./start_venv.sh

Install depedencies:
> pip install -r requirement.txt

Launch tests
> python -m pytest

# How to use 
- host
   - Internet needed
   - docker-compose up
   - sudo python server.py
- in vm
   - sudo gitlab-runner --debug run 


   