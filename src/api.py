from flask import Flask
from flask_restplus import Resource, Api, fields

app = Flask(__name__)
api = Api(app)

TODOS = {}

todo = api.model('Todo', {
    'task': fields.String(required=True, description='The task details')
})

parser = api.parser()
parser.add_argument('task', type=str, required=True, help='The task details', location='form')


def abort_if_todo_doesnt_exist(todo_id):
    if todo_id not in TODOS:
        api.abort(404, "Todo {} doesn't exist".format(todo_id))


@api.doc(responses={404: 'Todo not found'}, params={'todo_id': 'The Todo ID'})
@api.route('/<string:todo_id>', endpoint='/')
class Todo(Resource):
    @api.doc(description='todo_id should be in {0}'.format(', '.join(TODOS.keys())))
    @api.marshal_with(todo)
    def get(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        return TODOS[todo_id]

    @api.doc(parser=parser)
    @api.marshal_with(todo)
    def put(self, todo_id):
        args = parser.parse_args()
        task = {'task': args['task']}
        TODOS[todo_id] = task
        return task
    
    @api.doc(responses={200: ""})
    def delete(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        del TODOS[todo_id]
        return '', 200


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8082)
