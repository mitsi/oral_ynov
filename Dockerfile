FROM registry:5000/python:3.7-slim
COPY requirement.txt /app/requirement.txt
COPY src /app/src
WORKDIR /app
RUN pip install -r requirement.txt
EXPOSE 8082
CMD python /app/src/api.py