import pytest
from src import api


@pytest.fixture
def app():
    app = api.app
    return app


def test_return_not_found(client):
    response = client.get("todo")
    assert response.status_code == 404


def test_return_todo(client):
    api.TODOS["aze"] = {"task": "qsd"}
    response = client.get("aze")
    assert response.status_code == 200
    assert response.json == {'task': 'qsd'}

def test_delete_todo(client):
    api.TODOS["aze"] = {"task": "qsd"}
    response = client.delete("aze")
    print(response)
    assert response.status_code == 204
    assert response.data == b""

def test_return_not_found_on_delete_unknown_todo_id(client):
    response = client.delete("aze")
    assert response.status_code == 404
    assert response.json == {'message': "Todo aze doesn't exist"}